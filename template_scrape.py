import datetime
import time

from selenium import webdriver
from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options

from secret import uid, pwd

path = "/home/hank/Downloads/gecko_driver"

options = webdriver.FirefoxOptions()

# options.headless = True#False

driver = webdriver.Firefox(path, options=options)

url = "https://kanbanflow.com/board/oedD9S"
driver.get(url)


def kanban():
    try:
        user = driver.find_element_by_id("email")
        password = driver.find_element_by_id("password")
        user.clear()
        password.clear()
        user.send_keys(uid)
        password.send_keys(pwd)
        print("Made it past the login screen")
        time.sleep(3)
        # Login button
        driver.find_element_by_class_name("form-actionButton").click()
        time.sleep(5)
        # Menu button
        driver.find_element_by_class_name("boardHeader-menuButton").click()
        time.sleep(3)
        # Reports button
        driver.find_element_by_xpath(
            "/html/body/div[3]/div[2]/div[2]/button[2]"
        ).click()
        time.sleep(3)
        # Export button
        driver.find_element_by_xpath(
            "/html/body/div[3]/div[2]/div[2]/div/button[13]"
        ).click()
        time.sleep(3)
        # Selenium template button
        driver.find_element_by_xpath(
            "/html/body/div[7]/div[2]/div[2]/div/ul/li"
        ).click()
        # Download Excel file button
        driver.find_element_by_xpath("/html/body/ul[1]/li[1]").click()
        print("The file has either downloaded or has begun downloading.")
    except Exception as e:
        print(e)


kanban()
