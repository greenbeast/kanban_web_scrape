# Kanban Web Scrape

Simple Python script using Selenium to grab info from a website and download a spreadsheet of it.


# Update 12-02-2020
**Bruh...** Finally fixed this. Basically what I did was right click and `Inspect Element` then right click on the button you want to click and you can go to `Copy` then `Copy XPATH` and that makes life a million times easier. I didn't know that was an option until now. Fuck me, this absolutely made me hate web scraping because I couldn't figure it out but now I love it again. What a life we live.

### Update 12-04-2020

Had issues with Firefox because it kept asking me if I would like to
download the file or not which made it impossible to do as headless so
I switched it over to Chromium and just made it use that. I don't know
what the issue with Firefox was because I would set my preference as
automatically download this type of file but each new session with
selenium it would ask again so it didn't seem to be retaining the
info. Either way though, Chromium is now being used which is probably
good because it gives me a reason to keep it on my laptop. 

# Don't forget to add a script called `secret.py` with the UID and PWD.

# How to run (Windows)

To run this script you will need to have [chrome](https://www.google.com/chrome/) downloaded as well as the [chrome web driver](https://sites.google.com/a/chromium.org/chromedriver/downloads), and you will need Python 3.8(could probably be 3.7 or 3.9, I've just been using 3.8). You have to make sure the versions match or else the web driver wont work. So go in your Chrome browser click the three dots near the top right hand corner of the screen then towards the bottom of the drop down menu it *Help* and click on that then *About Google Chrome*. There it'll show you the version number you have (as of right now the up to date version is 87.0.xxxx.xx) so make sure to download the chrome driver that is the version number matching your Chrome version number. The web driver downloads as a zip file so extract it to `C:\Program Files (x86)\chromedriver.exe` for the script to find it easily. As of right now headless isn't working because Chrome is having issues going headless and still working(idk, seems to be an issue with the website). Next you'll want to download the code on your computer, if you have `git` installed you can run `git clone https://gitlab.com/greenbeast/kanban_web_scrape.git` in your command line and it'll clone the code into a folder called `kanban_web_scrape`. If you don't have `git` installed you can also download the code as a zip file by hitting the download button on this website and unzip it wherever is most convenient. Next you'll want to `cd` into that directory (so if you open a terminal/powershell the type `cd kanban_web_scrape` or whatever your path is, so if the kanban code is in `Downloads` you would use `cd Downloads` then hit enter and then `cd kanban_web_scrape`). Once you are in the directory you can run the code by using either `py chromium_web.py` or `python3 chromium_web.py`. If you hit `tab` then it will auto complete any command in the terminal. Now that it is running it'll automatically open chrome and controll is from there so feel free to run the code and do something else as it'll take about a minute to run. The spreadsheet will automatically download in the Downloads folder and will be called "Tech Support - Selenium". The current setting is to download the last weeks worth of notes but that can be changed if needed.