import time

from selenium import webdriver

from secret import uid, pwd

# Path for home laptop
# path = "/home/hank/Downloads/chrome_driver/chromedriver"

# Path for work computer
path = r"C:\Program Files (x86)\chromedriver.exe"

# Work Prefs
# prefs = {"download.default_directory": r"C:\Users\Hank\Documents\Random Python Scripts"}

# Home prefs
# prefs = {"download.default_directory": "/home/hank/Work/GnD/"}

options = (
    webdriver.ChromeOptions()
)  # Changed chrome_options to options because chrome_options is depreciated.

# options.headless = True  # False

options.add_argument("--no-sandbox")

# This should get rid of a depreciation error.
options.add_argument("--log-level=3")

options.add_argument("--disable-dev-shm-usage")

options.add_argument("disable-infobars")

options.add_argument("--disable-extensions")

options.add_argument(
    "--remote-debugging-port=9222"
)  # This fixed an issue with autodevport or something

# options.add_experimental_option("prefs", prefs)
# applies the above changes to the webdriver
driver = webdriver.Chrome(path, options=options)


url = "https://kanbanflow.com/board/oedD9S"
driver.get(url)


def kanban():
    try:
        user = driver.find_element_by_id("email")
        password = driver.find_element_by_id("password")
        user.clear()
        password.clear()
        user.send_keys(uid)
        password.send_keys(pwd)
        time.sleep(5)
        # Login button
        driver.find_element_by_xpath("/html/body/div/main/section/form/p[4]/button").click()
        # driver.find_element_by_class_name("form-actionButton").click()
        time.sleep(3)
        print("Made it past the login screen. This will take about 50 seconds to run.")
        # Menu button
        driver.find_element_by_class_name("boardHeader-menuButton").click()
        time.sleep(3)
        # Reports button
        driver.find_element_by_xpath(
            "/html/body/div[3]/div[2]/div[2]/button[2]"
        ).click()
        time.sleep(3)
        # Export button
        driver.find_element_by_xpath(
            "/html/body/div[3]/div[2]/div[2]/div/button[13]"
        ).click()
        time.sleep(3)
        # Selenium template button
        driver.find_element_by_xpath(
            "/html/body/div[7]/div[2]/div[2]/div/ul/li"
        ).click()
        # Download Excel file button
        driver.find_element_by_xpath("/html/body/ul[1]/li[1]").click()
        time.sleep(10)
        print("The file has either downloaded or has begun downloading.")
        driver.close()
    except Exception as e:
        print(e)


kanban()
